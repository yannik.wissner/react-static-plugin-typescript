import { readFileSync } from 'fs';
import path from 'path';
import { Configuration as WebpackConfiguration, RuleSetRule, RuleSetLoader } from 'webpack';
import ForkTsCheckerWebpackPlugin from 'fork-ts-checker-webpack-plugin';
import convertPathsToAliases from 'convert-tsconfig-paths-to-webpack-aliases';
import { parseConfigFileTextToJson, findConfigFile, sys } from 'typescript';

interface Options {
  typeCheck?: boolean;
  tsconfig?: string;
};

export default (options?: Options) => {
  return {
    config: getReactStaticConfigExtender(),
    webpack: getWebpackConfigExtender(options),
  };
};

interface ReactStaticArgs {
  stage: 'prod' | 'dev' | 'node';
  defaultLoaders: {
    jsLoader: RuleSetRule & { use: Array<RuleSetLoader & { options: object }>};
    jsLoaderExt: RuleSetRule;
    cssLoader: RuleSetRule;
    fileLoader: RuleSetRule;
  },
};

function getWebpackConfigExtender(options?: Options) {
  const tsconfigLocation = findConfigFile('./', sys.fileExists);
  const tsconfigPath = tsconfigLocation ? path.resolve(tsconfigLocation) : tsconfigLocation;
  const parsedTsConfig = tsconfigPath ? parseConfigFileTextToJson(tsconfigPath, readFileSync(tsconfigPath, 'utf8')) : {};
  let tsAliases: any | {};
  try {
    tsAliases = convertPathsToAliases(parsedTsConfig.config)
  } catch (_) {
    tsAliases = {}
  }

  return (previousConfig: WebpackConfiguration, args: ReactStaticArgs) => {
    const previousModuleConfig: any = previousConfig.module || {};
    const previousResolveConfig = previousConfig.resolve || {};
    const previousResolveExtensions = previousResolveConfig.extensions || [];

    const typescriptLoader = getTypeScriptLoader(args.defaultLoaders.jsLoader);

    const plugins = previousConfig.plugins || [];
    if ((options || {}).typeCheck !== false) {
      plugins.push(getTypecheckPlugin(tsconfigPath));
    }

    const loaders = (
      Array.isArray(previousModuleConfig.rules)
      && previousModuleConfig.rules.length > 0
      && Array.isArray(previousModuleConfig.rules[0].oneOf)
    )
    ? [ ...previousModuleConfig.rules[0].oneOf]
    : [args.defaultLoaders.jsLoader, args.defaultLoaders.jsLoaderExt, args.defaultLoaders.cssLoader, args.defaultLoaders.fileLoader];

    if (loaders.indexOf(args.defaultLoaders.jsLoader) !== -1) {
      // If the default Javascript loader is still present, replace it with the TypeScript loader:
      loaders[loaders.indexOf(args.defaultLoaders.jsLoader)] = typescriptLoader;
    } else {
      // Otherwise just add the TypeScript loader to the list of loaders, before the others:
      loaders.unshift(typescriptLoader);
    }

    return {
      ...previousConfig,
      plugins: plugins,
      resolve: {
        ...previousResolveConfig,
        extensions: previousResolveExtensions.concat(['.ts', '.tsx']),
        alias: {
          ...previousResolveConfig.alias,
          ...tsAliases,
        },
      },
      module: {
        ...previousModuleConfig,
        rules: [{
          oneOf: loaders,
        }],
      },
    };
  };
}

type ReactStaticConfig = {
  extensions: string[];
};
function getReactStaticConfigExtender() {
  return (previousConfig: ReactStaticConfig) => {

    return {
      ...previousConfig,
      extensions: previousConfig.extensions.concat(['.ts', '.tsx']),
    };
  };
}

function getTypeScriptLoader(jsLoader: RuleSetRule & { use: Array<RuleSetLoader & { options: object }>}) {
  const jsLoaderBabelPresets = jsLoader.use[0].options.presets || [];

  const typescriptLoader: RuleSetRule = {
    ...jsLoader,
    test: /\.(js|jsx|ts|tsx)$/,
    use: [
      {
        ...jsLoader.use[0],
        options: {
          ...jsLoader.use[0].options,
          presets: jsLoaderBabelPresets.concat('@babel/preset-typescript'),
        },
      },
    ],
  };

  return typescriptLoader;
}

function getTypecheckPlugin(tsconfigPath?: string) {
  return new ForkTsCheckerWebpackPlugin({
    async: false,
    checkSyntacticErrors: true,
    tsconfig: tsconfigPath,
  });
}
